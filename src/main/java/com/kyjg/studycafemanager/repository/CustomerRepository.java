package com.kyjg.studycafemanager.repository;

import com.kyjg.studycafemanager.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
