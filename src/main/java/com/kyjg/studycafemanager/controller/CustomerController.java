package com.kyjg.studycafemanager.controller;

import com.kyjg.studycafemanager.model.CustomerItem;
import com.kyjg.studycafemanager.model.CustomerRequest;
import com.kyjg.studycafemanager.model.CustomerTimeRequest;
import com.kyjg.studycafemanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerService customerService;
    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);

        return "OK";
    }
    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        List<CustomerItem> result = customerService.getCustomers();

        return result;
    }
    @PutMapping("/time/id/{id}")
    public String putCustomerTime(@PathVariable long id, @RequestBody @Valid CustomerTimeRequest request) {
        customerService.putCustomerTime(id, request);

        return "OK";
    }

    @DeleteMapping("/sign-out/id/{id}")
    public String delCustomerInfo(@PathVariable long id) {
        customerService.delCustomerInfo(id);

        return "OK";
    }
}
