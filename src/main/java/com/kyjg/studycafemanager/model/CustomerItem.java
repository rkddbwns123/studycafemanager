package com.kyjg.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerItem {
    private String customerName;
    private String customerPhone;
    private Integer partTimeTicket;
    private LocalDateTime entryTime;
    private LocalDateTime exitTime;
}
