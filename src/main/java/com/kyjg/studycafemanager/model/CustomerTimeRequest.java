package com.kyjg.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerTimeRequest {
    private LocalDateTime entryTime;
    @NotNull
    private Integer partTimeTicket;
    private LocalDateTime exitTime;
}
