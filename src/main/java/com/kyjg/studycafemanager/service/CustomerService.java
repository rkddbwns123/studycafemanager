package com.kyjg.studycafemanager.service;

import com.kyjg.studycafemanager.entity.Customer;
import com.kyjg.studycafemanager.model.CustomerItem;
import com.kyjg.studycafemanager.model.CustomerRequest;
import com.kyjg.studycafemanager.model.CustomerTimeRequest;
import com.kyjg.studycafemanager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer(CustomerRequest request) {
        Customer addData = new Customer();

        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setPartTimeTicket(request.getPartTimeTicket());
        addData.setEntryTime(LocalDateTime.now());
        addData.setExitTime(LocalDateTime.now().plusHours(request.getPartTimeTicket()));

        customerRepository.save(addData);
    }
    public List<CustomerItem> getCustomers() {
        List<Customer> originList = customerRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        for (Customer item : originList) {
            CustomerItem addData = new CustomerItem();
            addData.setCustomerName(item.getCustomerName());
            addData.setCustomerPhone(item.getCustomerPhone());
            addData.setPartTimeTicket(item.getPartTimeTicket());
            addData.setEntryTime(item.getEntryTime());
            addData.setExitTime(item.getExitTime());

            result.add(addData);
        }
        return result;
    }
    public void putCustomerTime(long id, CustomerTimeRequest request) {
        Customer originData = customerRepository.findById(id).orElseThrow();

        originData.setPartTimeTicket(request.getPartTimeTicket());
        originData.setExitTime(originData.getEntryTime().plusHours(request.getPartTimeTicket()));

        customerRepository.save(originData);
    }

    public void delCustomerInfo(long id) {
        customerRepository.deleteById(id);
    }
}